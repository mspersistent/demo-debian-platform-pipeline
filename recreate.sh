#!/bin/bash

DATE=$(date +%Y%m%d)
file="${DATE}.iso"
num=0
while [ -f $file ]; do
    num=$(( $num + 1 ))
    file="${DATE}-${num}.iso"
done
touch $file

#curl -LO# https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.8.0-amd64-netinst.iso
sudo apt -y install xorriso
#xorriso -osirrox on -indev debian-10.1.0-amd64-netinst.iso  -extract / isofiles/
xorriso -osirrox on -indev debian12.5_DVD.iso  -extract / isofiles/
chmod +w -R isofiles/install.amd/
gunzip isofiles/install.amd/initrd.gz
echo preseed.cfg | cpio -H newc -o -A -F isofiles/install.amd/initrd
gzip isofiles/install.amd/initrd
chmod -w -R isofiles/install.amd/
chmod a+w isofiles/isolinux/isolinux.bin
genisoimage -r -J -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o $file isofiles
echo "The build debian OS name : "$file
